'use strict';
~ function() {
    var $ = TweenMax,
        tl = new TimelineMax(),
        ad = document.getElementById('mainContent'),
        easeInOut = Power2.easeInOut,
        easeOut = Power3.easeOut,
        dcAd = {};

    window.init = function() {
        dcAd.setObjects();

    }

    //Function to run with any animations starting on load, or bringing in images etc
    dcAd.setObjects = function() {

        Enabler.addEventListener(studio.events.StudioEvent.ORIENTATION, dcAd.changeOrientationHandler);

        //Assign All the elements to the element on the page
        dcAd.container = document.getElementById('mainContent');
        // dcAd.bgExit = document.getElementById('background_exit_dc');

        // dcAd.bgImage1 = document.getElementById('bgImage1');
        // dcAd.bgImage2 = document.getElementById('bgImage2');

        // dcAd.cta = document.getElementById('cta_btn');

        // dcAd.addListeners();

        dcAd.changeOrientationHandler();

        // dcAd.container.style.visibility = 'visible';

        play();
    }

    //Add Event Listeners
    dcAd.addListeners = function() {
        dcAd.bgExit.addEventListener('click', dcAd.bgExitHandler, false);
        dcAd.cta.addEventListener('click', dcAd.ctaExitHandler, false);

        console.log("orientation mode: " + Enabler.getOrientation().getMode());
        console.log("orientation degrees: " + Enabler.getOrientation().getDegrees());
    }

    //exits
    dcAd.bgExitHandler = function(e) {
        dcAd.enablerExit("ON_BG_EXIT");
    }

    dcAd.ctaExitHandler = function(e) {
        dcAd.enablerExit("ON_CTA_EXIT");
    }

    //Function to call for Custom Exit Tracking - using switch method
    dcAd.enablerExit = function(type) {
        switch (type) {
            case "ON_CTA_EXIT":
                Enabler.exit('HTML5_CTA_Clickthrough');
                break;

            case "ON_BG_EXIT":
                Enabler.exit('HTML5_Background_Clickthrough');
                break;
        }
    }

    dcAd.changeOrientationHandler = function() {

        console.log("orientation mode: " + Enabler.getOrientation().getMode());
        console.log("orientation degrees: " + Enabler.getOrientation().getDegrees());

        if (Enabler.getOrientation().getMode() == 'portrait') {
            dcAd.container.style.width = 726 + 'px';
            dcAd.container.style.height = 1022 + 'px';

            // dcAd.bgImage1.style.display = 'inline';
            // dcAd.bgImage2.style.display = 'none';
        } else {
            dcAd.container.style.width = 1022 + 'px';
            dcAd.container.style.height = 726 + 'px';
            // dcAd.bgImage1.style.display = 'none';
            // dcAd.bgImage2.style.display = 'inline';
        }
    }

    function play() {

        tl.set(ad, { force3D: false })

        tl.addLabel('frameOne')
        tl.to('#copyOne', 0.5, { opacity: 1, ease: easeInOut }, 'frameOne')

        tl.addLabel('frameTwo', '+=1.8')
        tl.to(['#blueBg', '#copyOne', '#sonyLogoWhite'], 0.5, { opacity: 0, ease: easeInOut }, 'frameTwo')
        tl.to('#sonyLogoBlack', 0.5, { opacity: 1, ease: easeInOut }, 'frameTwo')
        tl.to('#frameTwoImg', 0.5, { x: 0, rotation: 0.01, ease: easeInOut }, 'frameTwo')
        tl.staggerTo('#copyTwo span', 0.5, { opacity: 1, y: 0, rotation: 0.01, ease: easeOut }, 0.15, 'frameTwo+=0.5')

        tl.addLabel('frameThree', '+=1.2')
        tl.to('#copyTwo', 0.5, { opacity: 0, rotation: 0.01, ease: easeInOut }, 'frameThree')
        tl.to('#imageThreeContainer', 0.5, { x: 0, rotation: 0.01, ease: easeInOut }, 'frameThree')
        tl.to('#frameTwoImg', 0.5, { opacity: 0, ease: easeInOut }, 'frameThree+=0.5')
        tl.staggerTo('#copyThree span', 0.5, { opacity: 1, y: 0, rotation: 0.01, ease: easeOut }, 0.15, 'frameThree+=0.5')

        tl.addLabel('frameFour', '+=1.2')
        tl.to('#copyThree', 0.5, { opacity: 0, rotation: 0.01, ease: easeInOut }, 'frameFour')
        tl.to('#endFrameImg', 0.5, { x: 0, rotation: 0.01, ease: easeInOut }, 'frameFour')
        tl.to('#endFrameCopyContainer', 0.5, { opacity: 1, ease: easeInOut }, 'frameFour+=0.5')
        tl.to('#sonyLogoWhite', 0.5, { opacity: 1, ease: easeInOut }, 'frameFour+=0.5')
        tl.to('#sonyLogoBlack', 0.5, { opacity: 0, ease: easeInOut }, 'frameFour+=0.5')
        tl.staggerTo('#endFrameCopy span', 0.5, { opacity: 1, y: 0, rotation: 0.01, ease: easeOut }, 0.15, 'frameFour+=1')
        tl.to('#endFrameTagline', 0.5, { opacity: 1, ease: easeInOut }, 'frameFour+=1.5')
        tl.to('#cta', 0.5, { opacity: 1, ease: easeInOut }, 'frameFour+=2')



    }

}();